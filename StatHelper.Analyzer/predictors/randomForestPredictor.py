from joblib import load

from predictors.predictor import Predictor


class RandomForestPredictor(Predictor):

    def load_model(self):
        return load('files/ml-models/random_forest.joblib')
