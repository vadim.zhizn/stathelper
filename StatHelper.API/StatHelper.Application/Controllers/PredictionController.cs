﻿using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Mvc;
using StatHelper.Application.Models;

namespace StatHelper.Application.Controllers;


[Route("[controller]")]
public class PredictionController : ControllerBase
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly JsonSerializerOptions _serializerOptions;
    private readonly PredictionServerConfig _predictionServerUrl;
    public PredictionController(
        IHttpClientFactory httpClientFactory,
        JsonSerializerOptions serializerOptions, 
        PredictionServerConfig predictionServerUrl)
    {
        _httpClientFactory = httpClientFactory;
        _serializerOptions = serializerOptions;
        _predictionServerUrl = predictionServerUrl;
    }

    [HttpPost]
    public async Task<IrisResult?> Predict(IrisCharacteristics irisCharacteristics)
    {
        var httpClient = _httpClientFactory.CreateClient();

        var json = JsonSerializer.Serialize(irisCharacteristics, _serializerOptions);
        var requestContent = new StringContent(json, Encoding.UTF8, "application/json");

        var response = await httpClient.PostAsync(_predictionServerUrl.Url, requestContent);

        var content = await response.Content.ReadAsStringAsync();
        return JsonSerializer.Deserialize<IrisResult>(content, _serializerOptions);
    }
}