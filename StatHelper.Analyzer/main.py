from flask import request, Flask, jsonify
from IrisCharacteristicsDto import IrisCharacteristicsDto
from IrisFactory import IrisFactory
from predictors.decisionTreePredictor import DecisionTreePredictor
from predictors.ligisticRegressionPredictor import LogisticRegressionPredictor
from predictors.randomForestPredictor import RandomForestPredictor

app = Flask(__name__)


@app.route('/', methods=['POST'])
def predict():
    json = request.get_json()
    model_name = json['predictionModel']

    predictor = get_model(model_name)

    iris_dto = IrisCharacteristicsDto(json)
    characteristics = iris_dto.get_characteristics_list()
    predicted = predictor.predict(characteristics)

    factory = IrisFactory(predicted)
    factory.load_desctiptions()
    iris = factory.get_iris(iris_dto)
    return jsonify(iris.to_dict())


def get_model(model_name):
    if model_name == "DecisionTree":
        return DecisionTreePredictor()
    if model_name == "LogisticRegression":
        return LogisticRegressionPredictor()
    return RandomForestPredictor()


if __name__ == '__main__':
    app.run(host='0.0.0.0')