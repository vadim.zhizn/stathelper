﻿namespace StatHelper.Application.Models;

public enum PredictionModel
{
    DecisionTree,
    LogisticRegression,
    RandomForest
}