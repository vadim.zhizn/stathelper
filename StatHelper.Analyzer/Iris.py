import IrisCharacteristicsDto


class Iris:

    def __init__(self, name: str, description: str, characteristics: IrisCharacteristicsDto):
        self.name = name
        self.description = description
        self.characteristics = characteristics

    def to_dict(self):
        return {"name": self.name, "description": self.description, "irisCharacteristics": self.characteristics.to_dict()}
