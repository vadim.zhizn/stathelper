import json

from Iris import Iris


class IrisFactory:

    def __init__(self, class_number):
        self.class_number = class_number
        self.descriptions_path = "files/iris_descriptions.json"
        self.setosa_description = ""
        self.versicolor_description = ""
        self.virginica_description = ""

    def load_desctiptions(self):
        f = open(self.descriptions_path, "r")
        descriptions = json.load(f)
        self.setosa_description = descriptions["setosa"]
        self.versicolor_description = descriptions["versicolor"]
        self.virginica_description = descriptions["virginica"]

    def get_iris(self, characteristics) -> Iris:
        if self.class_number == 0:
            return Iris("Setosa", self.setosa_description, characteristics)
        if self.class_number == 1:
            return Iris("Versicolor", self.versicolor_description, characteristics)
        if self.class_number == 2:
            return Iris("Virginica", self.virginica_description, characteristics)
