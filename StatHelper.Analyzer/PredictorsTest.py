import unittest

from predictors.decisionTreePredictor import DecisionTreePredictor
from predictors.ligisticRegressionPredictor import LogisticRegressionPredictor
from predictors.randomForestPredictor import RandomForestPredictor


class TestPredictor(unittest.TestCase):

    def test_decision_tree_returns_setosa(self):
        model = DecisionTreePredictor()
        result = model.predict([[5.1, 3.5, 1.4, 0.2]])
        self.assertEqual(0, result)

    def test_logistic_regression_returns_setosa(self):
        model = LogisticRegressionPredictor()
        result = model.predict([[5.1, 3.5, 1.4, 0.2]])
        self.assertEqual(0, result)

    def test_random_forest_returns_setosa(self):
        model = RandomForestPredictor()
        result = model.predict([[5.1, 3.5, 1.4, 0.2]])
        self.assertEqual(0, result)

    def test_decision_tree_returns_versicolor(self):
        model = DecisionTreePredictor()
        result = model.predict([[7.0, 3.2, 4.7, 1.4]])
        self.assertEqual(1, result)

    def test_logistic_regression_returns_versicolor(self):
        model = LogisticRegressionPredictor()
        result = model.predict([[7.0, 3.2, 4.7, 1.4]])
        self.assertEqual(1, result)

    def test_random_forest_returns_versicolor(self):
        model = RandomForestPredictor()
        result = model.predict([[7.0, 3.2, 4.7, 1.4]])
        self.assertEqual(1, result)

    def test_decision_tree_returns_virginica(self):
        model = DecisionTreePredictor()
        result = model.predict([[6.3, 3.3, 6.0, 2.5]])
        self.assertEqual(2, result)

    def test_logistic_regression_returns_virginica(self):
        model = LogisticRegressionPredictor()
        result = model.predict([[6.3, 3.3, 6.0, 2.5]])
        self.assertEqual(2, result)

    def test_random_forest_returns_virginica(self):
        model = RandomForestPredictor()
        result = model.predict([[6.3, 3.3, 6.0, 2.5]])
        self.assertEqual(2, result)

if __name__ == '__main__':
    unittest.main()
