from joblib import load
from abc import ABC, abstractmethod


class Predictor(ABC):

    def predict(self, characteristics):
        loaded_model = self.load_model()
        return loaded_model.predict(characteristics)

    @abstractmethod
    def load_model(self):
        """loads classifier model"""

