import unittest

from IrisCharacteristicsDto import IrisCharacteristicsDto


class TestIrisCharacteristics(unittest.TestCase):

    def test_returns_valid_list(self):
        test_data = {
            "sepalLength" : 1,
            "sepalWidth" : 2,
            "petalLength" : 3,
            "petalWidth" : 4
        }

        characteristics_list = IrisCharacteristicsDto(test_data).get_characteristics_list()
        self.assertEqual([[1, 2, 3 ,4 ]], characteristics_list)