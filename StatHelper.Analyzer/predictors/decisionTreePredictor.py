from joblib import load

from predictors.predictor import Predictor


class DecisionTreePredictor(Predictor):

    def load_model(self):
        return load('files/ml-models/decision_tree.joblib')