class IrisCharacteristicsDto:
    def __init__(self, dictionary):
        self.sepalLength = dictionary['sepalLength']
        self.sepalWidth = dictionary['sepalWidth']
        self.petalLength = dictionary['petalLength']
        self.petalWidth = dictionary['petalWidth']

    def get_characteristics_list(self):
        return [[self.sepalLength, self.sepalWidth, self.petalLength, self.petalWidth]]

    def to_dict(self):
        return {"sepalLength" : self.sepalLength, "sepalWidth" : self.sepalWidth, "petalLength" : self.petalLength, "petalWidth" : self.petalWidth}