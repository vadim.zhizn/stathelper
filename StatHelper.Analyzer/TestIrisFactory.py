import unittest

from IrisFactory import IrisFactory


class TestIrisFactory(unittest.TestCase):

    def test_factory_returns_setosa(self):
        test_characteristics = [[1, 2, 3, 4]]
        factory = IrisFactory(0)
        iris = factory.get_iris(test_characteristics)

        self.assertEqual("Setosa", iris.name)
        self.assertEqual(test_characteristics, iris.characteristics)

    def test_factory_returns_setosa(self):
        test_characteristics = [[1, 2, 3, 4]]
        factory = IrisFactory(1)
        iris = factory.get_iris(test_characteristics)

        self.assertEqual("Versicolor", iris.name)
        self.assertEqual(test_characteristics, iris.characteristics)

    def test_factory_returns_setosa(self):
        test_characteristics = [[1, 2, 3, 4]]
        factory = IrisFactory(2)
        iris = factory.get_iris(test_characteristics)

        self.assertEqual("Virginica", iris.name)
        self.assertEqual(test_characteristics, iris.characteristics)