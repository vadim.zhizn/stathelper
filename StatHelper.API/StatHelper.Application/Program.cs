using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using StatHelper.Application;

var builder = WebApplication.CreateBuilder(args);
var predictionServerUrl = new PredictionServerConfig();

builder.Configuration.GetSection("PredictionServerConfig").Bind(predictionServerUrl);
builder.Services.AddSingleton(predictionServerUrl);
builder.Services.AddHttpClient();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers().AddJsonOptions(options =>
    options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase);

var serializerOptions = new JsonSerializerOptions()
{
    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
    WriteIndented = true,
};
serializerOptions.Converters.Add(new JsonStringEnumConverter());
builder.Services.AddSingleton(serializerOptions);

var app = builder.Build();

app.UseSwagger()
    .UseSwaggerUI();
app.MapDefaultControllerRoute();

app.Run();