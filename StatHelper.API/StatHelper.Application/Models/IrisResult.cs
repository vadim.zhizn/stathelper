﻿namespace StatHelper.Application.Models;

public class IrisResult
{
    public string Name { get; set; } = null!;
    public string Description { get; set; } = null!;
    public IrisCharacteristics IrisCharacteristics { get; set; } = null!;
}