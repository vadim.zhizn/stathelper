﻿using System.Diagnostics.CodeAnalysis;

namespace StatHelper.Application.Models;

public class IrisCharacteristics
{
    public double SepalLength { get; set; }
    public double SepalWidth { get; set; }
    public double PetalLength { get; set; }
    public double PetalWidth { get; set; }

    public PredictionModel PredictionModel { get; set; } = PredictionModel.DecisionTree;
}