from joblib import load

from predictors.predictor import Predictor


class LogisticRegressionPredictor(Predictor):

    def load_model(self):
        return load('files/ml-models/logistic_regression.joblib')